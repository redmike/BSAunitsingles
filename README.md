

# BSA part & motorcycle online store.

<img alt="hero image" src="https://miguel.debloat.us/static/images/fullstack/hero.png" width="400"/>

<img alt="adding a item to the cart" src="https://miguel.debloat.us/static/images/fullstack/item.png" width="400"/>



## FrontEnd
- React, styled components, some sass
- Redux
- JWT

## BackEnd
- MongoDB
- Express
- Node

## Payments
- Stripe

### About
I scraped (with python) an entire real database, manipulate the data, converted to JSON and pushed to my own server. 
The not great, is the real page was changing some items, so it can be some broken images (because I took the image as a link, to maintain my db light).
You can mess around in the site, and don't worry if you get exited and bought something by mistake, it will no me charged to the credit card, neither you will receive the item.
